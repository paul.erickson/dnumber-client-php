<?php

use edu\wisc\doit\financials\dnumber\DnumberClient;
use edu\wisc\doit\financials\dnumber\MockDnumberClient;
use edu\wisc\doit\financials\dnumber\DnumberResponse;

/**
 * Tests for {@link MockDnumberClient}.
 */
class MockDnumberClientTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    function retrieveByIdActiveByDefault()
    {
        /** @var DnumberClient $client */
        $client = new MockDnumberClient();
        /** @var DnumberResponse $response */
        $response = $client->retrieveById("D000123");
        static::assertTrue($response->isActive());
    }

    /**
     * @test
     */
    function retrieveByIdInactive()
    {
        /** @var DnumberClient $client */
        $client = new MockDnumberClient(false);
        /** @var DnumberResponse $response */
        $response = $client->retrieveById("D000123");
        static::assertFalse($response->isActive());
    }

}