<?php

namespace edu\wisc\doit\financials\dnumber;

/**
 * DnumberException represents an exception that occurred contacting the DNumber API.
 */
class DnumberException extends \Exception
{

}
