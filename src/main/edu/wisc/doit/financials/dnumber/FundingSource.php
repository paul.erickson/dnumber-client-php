<?php

namespace edu\wisc\doit\financials\dnumber;

/**
 * FundingSource encapsulates the data returned by the "fundingSource" key from the DoIT Number API.
 */
class FundingSource
{

    /** @var string */
    private $campusCode;

    /** @var string */
    private $dds;

    /** @var string */
    private $fund;

    /** @var string */
    private $project;

    /** @var string */
    private $program;

    /** @var string */
    private $account;

    /** @var string */
    private $udds;

    /** @var string */
    private $groupDisplayName;

    function __construct(
        $campusCode,
        $dds,
        $fund,
        $project,
        $program,
        $account,
        $udds,
        $groupDisplayName
    )
    {
        $this->campusCode = $campusCode;
        $this->dds = $dds;
        $this->fund = $fund;
        $this->project = $project;
        $this->program = $program;
        $this->account = $account;
        $this->udds = $udds;
        $this->groupDisplayName = $groupDisplayName;
    }

    /**
     * @return string
     */
    public function getCampusCode()
    {
        return $this->campusCode;
    }

    /**
     * @return string
     */
    public function getDds()
    {
        return $this->dds;
    }

    /**
     * @return string
     */
    public function getFund()
    {
        return $this->fund;
    }

    /**
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return string
     */
    public function getUdds()
    {
        return $this->udds;
    }

    /**
     * @return string
     */
    public function getGroupDisplayName()
    {
        return $this->groupDisplayName;
    }

}
