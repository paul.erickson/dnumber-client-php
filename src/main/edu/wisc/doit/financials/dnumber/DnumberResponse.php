<?php

namespace edu\wisc\doit\financials\dnumber;

/**
 * DnumberResponse represents the response returned by the DoIT Number API.
 */
class DnumberResponse
{

    /** @var string */
    private $id;

    /** @var string */
    private $reference;

    /** @var FundingSource */
    private $fundingSource;

    /** @var \DateTime */
    private $startDate;

    /** @var \DateTime */
    private $endDate;

    /** @var \DateTime */
    private $lastModified;

    /** @var string */
    private $lastModifiedBy;

    /** @var bool */
    private $active;

    /**
     * DnumberResponse constructor.
     * @param string $id
     * @param string|null $reference
     * @param FundingSource $fundingSource
     * @param \DateTime $startDate
     * @param \DateTime|null $endDate
     * @param \DateTime $lastModified
     * @param string $lastModifiedBy
     * @param bool $active
     */
    public function __construct(
        $id,
        $reference,
        $fundingSource,
        \DateTime $startDate,
        \DateTime $endDate,
        \DateTime $lastModified,
        $lastModifiedBy,
        $active
    ) {
        $this->id = $id;
        $this->reference = $reference;
        $this->fundingSource = $fundingSource;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->lastModified = $lastModified;
        $this->lastModifiedBy = $lastModifiedBy;
        $this->active = $active;
    }

    /**
     * Get this DoIT Number's ID
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get this DoIT Number's reference
     * @return string
     */
    function getReference()
    {
        return $this->reference;
    }

    /**
     * Get this DoIT Number's funding source
     * @return FundingSource
     */
    function getFundingSource()
    {
        return $this->fundingSource;
    }

    /**
     * Get the date that this DoIT Number last became active
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Get the date that this DoIT Number will become inactive
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Get the date that this DoIT Number was last modified
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Get the NetID of the person who last modified this DoIT Number
     * @return string
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Determine whether this DoIT Number is active or not
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
